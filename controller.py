'''
    程序控制：
        猜字游戏：
            1.系统产生一个随机数
            2.用户从键盘输入猜的数字，
                如果猜的过大，提示过大
                如果猜的过小，提示过小
                正好猜中。终止此游戏。
        1.循环: while
        2.判断：if
        3.输入：input
        4.随机数工具：random
        5.变量
'''
# a = 6
# b = 6
#
#
#
# print(a + b)
#
# score = input("请输入一个数字：")# "12"   --->  12
# score = int(score)  # "12" -->   12  = score
# if score >= 90 and score <= 100:
#     print("优秀！")
# elif score >= 80  and score < 90:
#     print("良好")
# elif  score >= 70 and score < 80:
#     print("好！")
# elif score >= 60 and score < 70:
#     print("及格！")
# elif score >= 0 and score < 60:
#     print("对不起，不及格！")
# else:
#     print("数据非法，请重新输入！别瞎弄！")

#  while
'''
    1~10
    
    开始：1
    结束：10
    自增：1
    任务：打印当前这个数
'''
# i = 1 # 起始变量     判断比较多
# while i <= 10:
#     # 任务
#     print(i)
#     i =  i + 1

# for i in range(1,11):  # 有次数限制，优先使用次数循环
#     print(i)

import random  # 导入random模块，然后进行使用

num = random.randint(0,100)  # num当成系统随机数
count = 5000
for i in range(1,35):
    a = input("请输入你要猜的数字：")
    a = int(a) # 将键盘输入的字符，转换成数字 "32" -> 32

    if a > num and count >= 500:
        count = count - 500
        print("大了",count,i)
    elif a < num and count >= 500:
        count = count - 500
        print("小了",count,i)
    elif count < 500:
        print("金额不足，请充值！您本次用了", i, "次机会！")
        break
    else :
        count = count + 3000
        print("恭喜你，猜中了！本次幸运数字为：", num, "！您本次用了", i, "次机会！金币数量为:", count, "")
        p = "yes"
        r = input("是否继续(填写yes或no)：" )
        if r == p:
            continue
        else:
            print("游戏结束！ ！您本次用了", i, "次机会！")
        break


'''
    游戏：本金5000金币。
        每猜错一次：扣500，
        猜对，奖励3000金币。
        一直金币输完为止。
'''













